﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace Exercise_Alior
{
    class Credit_Form_Test
    {
        IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {
            driver = new FirefoxDriver();
        }

        [Test]
        public void test_phone_number_validation()
        {
            driver.Url = "https://wnioski.aliorbank.pl/spinner-process/?partnerId=POR_P_ZERO_S&transactionCode=pozyczki";

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            var cookies_element = driver.FindElement(By.XPath("/html/body/div[2]/root/div[4]/div/div[2]/div/a"));
            cookies_element.Click();

            var element_firstname = driver.FindElement(By.XPath("//*[@id=\"firstName\"]"));
            element_firstname.Click();
            element_firstname.SendKeys("imie");

            var element_lastname = driver.FindElement(By.XPath("//*[@id=\"lastName\"]"));
            element_lastname.Click();
            element_lastname.SendKeys("nazwisko");

            var element_email = driver.FindElement(By.XPath("//*[@id=\"emailAddress\"]"));
            element_email.Click();
            element_email.SendKeys("imie.nazwisko@gmail.com");

            var element_mobile = driver.FindElement(By.XPath("//*[@id=\"mobileNumber mobileNumber\"]"));
            element_mobile.Click();
            element_mobile.SendKeys("000000000");

            var element_cashamount = driver.FindElement(By.XPath("//*[@id=\"cashAmount cashAmount\"]"));
            element_cashamount.Click();
            element_cashamount.SendKeys("500");

            var element_year = driver.FindElement(By.XPath("/html/body/div[2]/root/div[1]/external/div/modal/div/div/div[2]/external-spinner/div/main/steps/step/spinner-contact-data/div[2]/form/div/ng-transclude/div[6]/div/div[2]/div[1]/div/custom-select/div/span/div/div[1]"));
            element_year.Click();

            var element_dropdownlist_year = driver.FindElement(By.XPath("/html/body/div[2]/root/div[1]/external/div/modal/div/div/div[2]/external-spinner/div/main/steps/step/spinner-contact-data/div[2]/form/div/ng-transclude/div[6]/div/div[2]/div[1]/div/custom-select/div/span/div/div[2]/div/div[2]"));
            element_dropdownlist_year.Click();

            var element_month = driver.FindElement(By.XPath("/html/body/div[2]/root/div[1]/external/div/modal/div/div/div[2]/external-spinner/div/main/steps/step/spinner-contact-data/div[2]/form/div/ng-transclude/div[6]/div/div[2]/div[3]/div/custom-select/div/span/div/div[1]"));
            element_month.Click();

            var element_dropdownlist_month = driver.FindElement(By.XPath("/html/body/div[2]/root/div[1]/external/div/modal/div/div/div[2]/external-spinner/div/main/steps/step/spinner-contact-data/div[2]/form/div/ng-transclude/div[6]/div/div[2]/div[3]/div/custom-select/div/span/div/div[2]/div/div[12]"));
            element_dropdownlist_month.Click();

            var element_agreement = driver.FindElement(By.XPath("/html/body/div[2]/root/div[1]/external/div/modal/div/div/div[2]/external-spinner/div/main/steps/step/spinner-contact-data/div[2]/form/div/ng-transclude/div[8]/div/spinner-agreement/fieldset/div[2]/label"));
            element_agreement.Click();

            var element_agreement1 = driver.FindElement(By.XPath("/html/body/div[2]/root/div[1]/external/div/modal/div/div/div[2]/external-spinner/div/main/steps/step/spinner-contact-data/div[2]/form/div/ng-transclude/div[8]/div/spinner-agreement/fieldset/div[2]/div/div[1]/div[2]/label"));
            element_agreement1.Click();

            var element_agreement2 = driver.FindElement(By.XPath("/html/body/div[2]/root/div[1]/external/div/modal/div/div/div[2]/external-spinner/div/main/steps/step/spinner-contact-data/div[2]/form/div/ng-transclude/div[8]/div/spinner-agreement/fieldset/div[2]/div/div[2]/div[2]/label"));
            element_agreement2.Click();

            var element_button = driver.FindElement(By.CssSelector("#app > div.view-app > root > div.clearfix > external > div > modal > div > div > div.modal-content > external-spinner > div > main > steps > step > spinner-contact-data > div.step-form > form > div > ng-transclude > buttons-control-bottom > div > div > button"));
            element_button.Click();

            // if element is not found it will rise an error
            var element_error = driver.FindElement(By.XPath("/html/body/div[2]/root/div[1]/external/div/modal/div/div/div[2]/external-spinner/div/main/steps/step/spinner-contact-data/div[2]/form/div/ng-transclude/div[3]/div/div[2]/div[2]/fieldset/span"));
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Close();
        }
    }
}
